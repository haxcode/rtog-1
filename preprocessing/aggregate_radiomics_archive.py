import sys
import os
import shutil
import gzip
from tqdm import tqdm 

segmentation_raw_path = "/local-scratch/marcthib_scratch/small_resolution_segmentation"
out_zip_root = "/local-scratch/marcthib_scratch/"

def unzip_gz(file_gz, file_out):
    with gzip.open(file_gz, 'rb') as f_in:
        with open(file_out, 'wb') as f_out:
            shutil.copyfileobj(f_in, f_out)


def main(args):
    # Handling input segmentation type
    try:
        segmentation_type = sys.argv[1]
    except IndexError:
        print("Error, should provide an argument as `python3 aggregate_radiomics_archive.py core`.")
        return 1
    
    try:
        assert segmentation_type in ["core", "full", "edema", "necrosis", "enhancing"]
    except AssertionError:
        print("Error, segmentation type ({}) is not among core, full, edema, necrosis, enhancing.".format(segmentation_type))
        return 1
    
    # Creating output folder 
    out_zip_folder = os.path.join(out_zip_root, "segmentation_" + segmentation_type)
    os.mkdir(out_zip_folder)
    patients = [s for s in os.listdir(segmentation_raw_path) if s != ".DS_Store"]
    # Iterating through patients
    for patient in tqdm(patients):
        patient_outpath = os.path.join(out_zip_folder, patient)
        os.mkdir(patient_outpath)
        
        segmentation_file = os.path.join(segmentation_raw_path, patient, patient + segmentation_type + ".nii.gz")
        volume_t1post_file = os.path.join(segmentation_raw_path, patient, patient + "t1post" + ".nii.gz")
        # Copying segmentation and volume file to output folder
        shutil.copy(segmentation_file, os.path.join(patient_outpath, "SEG_" + patient + "_" + segmentation_type + ".nii.gz"))
        shutil.copy(volume_t1post_file, os.path.join(patient_outpath, "volume_t1post.nii.gz"))
        # Uncompressing segmentation and volume files
        unzip_gz(os.path.join(patient_outpath, "SEG_" + patient + "_" + segmentation_type + ".nii.gz"), 
                        os.path.join(patient_outpath, "SEG_" + patient + "_" + segmentation_type + ".nii"))
        unzip_gz(os.path.join(patient_outpath, "volume_t1post.nii.gz"),
                       os.path.join(patient_outpath, "volume_t1post.nii"))
        # Removing compressed files
        os.remove(os.path.join(patient_outpath, "SEG_" + patient + "_" + segmentation_type + ".nii.gz"))
        os.remove(os.path.join(patient_outpath, "volume_t1post.nii.gz"))
        
    # Ziping output folder
    shutil.make_archive(out_zip_folder, 'zip', out_zip_folder)
    # Removing folder
    shutil.rmtree(out_zip_folder)
    

if __name__ == "__main__":
    main(sys.argv)
    
    