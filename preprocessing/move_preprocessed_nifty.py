## This scripts copies the pre-processed NIFTY files of RTOG, from 
## /local-scratch/marcthib_scratch/rtog_proc/out67694/919t1c_proc.nii 
## /local-scratch/marcthib_scratch/rtog_proc/out67694/919flair_proc.nii 
## to 
## /local-scratch/marcthib_scratch/rtog_proc/post/919/t1c_proc.nii
## /local-scratch/marcthib_scratch/rtog_proc/post/919/flair_proc.nii

import os
import sys
import shutil
from tqdm import tqdm

preproc_output_path = "/local-scratch/marcthib_scratch/rtog_proc"
moveto_path = "/local-scratch/marcthib_scratch/rtog_proc/post"

## Loop through the output folders of the preprocessing
for outfolder in os.listdir(preproc_output_path):
    outfolder_path = os.path.join(preproc_output_path, outfolder)
    available_files = os.listdir(outfolder_path)
    useful_available_files = [s for s in available_files if ("t1c_proc.nii" in s) or ("flair_proc.nii" in s)]
    
    # Isolate patients which have data available in this folder
    patients = set([s[:-12] for s in useful_available_files if "t1c_proc.nii" in s] + 
                      [s[:-14] for s in useful_available_files if ("flair_proc.nii" in s)])
    
    for patient in patients:
        out_patient_folder = os.path.join(moveto_path, patient)
        try:
            print("Creating directory", out_patient_folder)
            os.mkdir(out_patient_folder)
        except FileExistsError:
            pass
        
        print("Copying", os.path.join(outfolder_path, patient + "t1c_proc.nii"), "to", os.path.join(out_patient_folder, "t1c_proc.nii"))
        shutil.copy(os.path.join(outfolder_path, patient + "t1c_proc.nii"), 
                    os.path.join(out_patient_folder, "t1c_proc.nii"))

        print("Copying", os.path.join(outfolder_path, patient + "flair_proc.nii"), "to", os.path.join(out_patient_folder, "flair_proc.nii"))
        shutil.copy(os.path.join(outfolder_path, patient + "flair_proc.nii"), 
                    os.path.join(out_patient_folder, "flair_proc.nii"))
