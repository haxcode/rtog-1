import os
import sys
import shutil
from tqdm import tqdm
from utils.process_rtog_nii import process_rtog_nii
from multiprocessing import Pool

## Recovering from server restart : finding out which patients have been preprocessed already
preproc_output_path = "/local-scratch/marcthib_scratch/rtog_proc"
already_proc_patients = []
for outn in os.listdir(preproc_output_path):
    outnpath = os.path.join(preproc_output_path, outn)
    files = os.listdir(outnpath)
    for file in files:
        if "flair_proc.nii" in file:
            patientn = file.strip('flair_proc.nii')
            if (patientn + "t1c_proc.nii") in files:
                already_proc_patients.append(patientn)

## list already_proc_patients have been preprocessed already.


all_patients = os.listdir("/local-scratch/marcthib_scratch/rtog_nii_export")
all_patients = [s for s in all_patients if s != ".DS_Store"]

def path_to_first_nii(path):
    files = os.listdir(path)
    nii_files = [s for s in files if ".nii" in s]
    return os.path.join(path, nii_files[0])

def preproc_patient(patient_str):
    try:
        if int(patient_str) <= -1:
            return
        if patient_str in already_proc_patients:
            return

        # preparing directory as a buffer for the preprocessing
        print("Pre-processing patient", patient_str)
        out_dir = os.path.join("/local-scratch/marcthib_scratch/rtog_proc/", 'out' + str(os.getpid()))
        try:
            os.mkdir(out_dir)
        except FileExistsError:
            pass
        
        shutil.copy(path_to_first_nii(os.path.join("/local-scratch/marcthib_scratch/rtog_nii_export/", patient_str, 't1post')), 
                    os.path.join(out_dir, patient_str + 't1post.nii.gz'))
#        print("Copied ", os.path.join("/local-scratch/marcthib_scratch/rtog_nii/", patient_str, 't1post'))
#        print("to ", os.path.join(out_dir, patient_str + 't1post.nii'))
        shutil.copy(path_to_first_nii(os.path.join("/local-scratch/marcthib_scratch/rtog_nii_export/", patient_str, 'flair')), 
                    os.path.join(out_dir, patient_str + 'flair.nii.gz'))
#        print("Copied ", os.path.join("/local-scratch/marcthib_scratch/rtog_nii/", patient_str, 'flair'))
#        print("to ", os.path.join(out_dir, patient_str + 'flair.nii'))
        process_rtog_nii(out_dir,
                         input_t1c_filename=patient_str + 't1post.nii.gz', 
                         output_t1c_filename=os.path.join(out_dir, patient_str + 't1c_proc.nii'),
                         input_flair_filename=patient_str + 'flair.nii.gz', 
                         output_flair_filename=os.path.join(out_dir, patient_str + 'flair_proc.nii'),)
    except Exception as e:
        print(">> marc-tag-failed ", "Failed to convert patient ", patient_str, "\n >> ", e)
    

with Pool(40) as p:
    p.map(preproc_patient, sorted(all_patients, key=lambda x:int(x)))
