from tqdm import tqdm

import os
import shutil
import subprocess

folder_dcm_input = "/local-scratch/marcthib_scratch/rtog_dcm_export"
folder_nii_output = "/local-scratch/marcthib_scratch/rtog_nii_export"

patients = os.listdir(folder_dcm_input)
patients = [p for p in patients if p != ".DS_Store"]

def compose_dcm2niix_command(input_path, output_path):
    assert os.path.isdir(input_path)
    assert os.path.isdir(output_path)
    command = "dcm2niix -b y -z y -x n -t n -m n -o {} -s n -v n {}"
    return command.format(output_path, input_path.replace(" ", "\ ")\
                                                  .replace("&", "\&")\
                                                  .replace("(", "\(")\
                                                 .replace(")", "\)")\
                                                 .replace("'", "\\'"))

def dcm2niix(input_path, output_path, verbose=False):
    command = compose_dcm2niix_command(input_path, output_path)
    if verbose:
        print("[]$ ", command)
        print(subprocess.check_output(command, shell=True).decode("utf-8"))
    else:
        subprocess.check_output(command, shell=True).decode("utf-8")


for modality in ["flair", "t1post"]:
    for patient in tqdm(patients):
        print("patient:", patient)
        dcm_path = os.path.join(folder_dcm_input, patient, modality)
        if modality == "flair":
            os.mkdir(os.path.join(folder_nii_output, patient))
        nii_path = os.path.join(folder_nii_output, patient, modality)
        os.mkdir(nii_path)

        try:
            dcm2niix(dcm_path, nii_path, verbose=True)
            output_files = os.listdir(nii_path)
            output_json = [s for s in output_files if ".json" in s][0]
            os.remove(os.path.join(nii_path, output_json))
        except subprocess.CalledProcessError:
            print(">>Failed to run>> " + compose_dcm2niix_command(dcm_path, nii_path))
