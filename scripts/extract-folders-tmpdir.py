import os
import multiprocessing

from tqdm import tqdm, tqdm_notebook
import numpy as np

import pydicom


data_dir = '/labs/gevaertlab/users/hackhack/RTOG/RTOG_duplicate/0825-6686 DAR 2of2/DICOM_STORE_TEMP'

def extract_unique_patients(tup):
    directory, subs, files = tup
    unique_patient_ids = set()
    
    last_folder = directory.split("/")[-1]
    
    if (len(files) == 0) and (len(subs)) > 0:
        return "discard_" + directory
        
    if len(files) == 0:
        return "empty_" + directory
    
    for f in files:
        if len(f) < 4:
            continue
        if f[-4:] == '.dcm':
            return "full_" + directory
    return "nodcm_" + directory



def files_it():
    bar = tqdm(total=46204)
    for (directory, subs, files), _ in zip(os.walk(data_dir), range(300000)):
        bar.update(1)
        yield (directory, subs, files)
        
        

with multiprocessing.Pool(processes=30) as pool:
    s = pool.map(extract_unique_patients, files_it())

with open("tmp_folders_statuses_2.txt", "w") as f:
    for p in s:
        f.write(p + "\n")